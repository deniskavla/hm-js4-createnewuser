// Опишите своими словами, что такое метод обьекта
// Метод это как бы функция которая хранятся как свойство объекта.
// по данному примеру видно что в методе мы описываем действия над объектом который находится в функции.

function createNewUser() {
    let fName = prompt("Enter First Name", 'Ivan');
    // сделал по умолчанию данные что бы не вбивать посстоянно, я думаю в таких примерах такое не считается ошибкой?
    let lName = prompt("Enter last Name", 'Ivanov');
    let newUser = {
        "firstName": fName,
        "lastName": lName,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        setFirstName(firstName) {
            Object.defineProperty(newUser, "firstName", { value: "Denis"})
        },
        setLastName(lastName) {
            Object.defineProperty(newUser, "lastName", { value: "Shapochkin"})
        }
    }
    Object.defineProperty(newUser, 'firstName' , {
        configurable: true,
        writable: false,
        enumerable: true,
    });
    Object.defineProperty(newUser, 'lastName', {
        configurable: true,
        writable: false,
        enumerable: true,
    });

    return newUser;
}

const user = createNewUser();
alert(user.firstName); //oldName
alert(user.lastName); //oldlastName

user.setFirstName('Denis');
alert(user.firstName); // Denis newName

user.setLastName('Shapochkin');
alert(user.lastName); // Shapochkin newName

